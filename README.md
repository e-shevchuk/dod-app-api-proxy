# Recipe app API Proxy

NGINX proxy app for our recipe app API

## Usage

### Evnironment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Host name of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)